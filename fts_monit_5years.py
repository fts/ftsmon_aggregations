from elasticsearch import Elasticsearch, TransportError
import subprocess
import requests
import json
import datetime
import os

# Credentials for aggregated data in ES (r/w)
USER_ES = (os.environ['USER_ES'])
PASSWORD_ES = (os.environ['PASSWORD_ES'])
# Credentials for reading monit ES
USER_MONIT = (os.environ['USER_MONIT'])
PASSWORD_MONIT = (os.environ['PASSWORD_MONIT'])

URI_FTS_RAW_STATE = 'https://'+USER_MONIT+':'+PASSWORD_MONIT+'@es-monit.cern.ch/es/monit_prod_fts_raw_state*/_search'
URI_FTS_RAW_START = 'https://'+USER_MONIT+':'+PASSWORD_MONIT+'@es-monit.cern.ch/es/monit_prod_fts_raw_start*/_search'
URI_FTS_RAW_QUEUE = 'https://'+USER_MONIT+':'+PASSWORD_MONIT+'@es-monit.cern.ch/es/monit_prod_fts_raw_queue*/_search'

TYPE_FTS_STAGING_GENERAL = 'fts_agg_staging_general'
TYPE_FTS_STAGING_ERROR_REASON = 'fts_agg_staging_error_reason'
TYPE_FTS_WRITE_TAPE = 'fts_agg_write_tape'
TYPE_FTS_QUEUE_SUBMITTED = 'fts_agg_queue_submitted_requests'
TYPE_FTS_QUEUE_TIME = 'fts_agg_queue_time'
TYPE_FTS_OPTIMIZER = 'fts_agg_optimizer'

URIS =[URI_FTS_RAW_STATE, URI_FTS_RAW_START, URI_FTS_RAW_QUEUE]

TYPES_FTS_RAW_STATE = [TYPE_FTS_STAGING_GENERAL, TYPE_FTS_STAGING_ERROR_REASON, TYPE_FTS_QUEUE_SUBMITTED, TYPE_FTS_QUEUE_TIME]
TYPES_FTS_RAW_START = [TYPE_FTS_WRITE_TAPE]
TYPES_FTS_RAW_QUEUE = [TYPE_FTS_OPTIMIZER]

def launch_queries():
	headers = { 'Content-Type': 'application/json' }
	for uri in URIS:
		if uri == URI_FTS_RAW_STATE:
			for index in TYPES_FTS_RAW_STATE:
				timestamp_start = str(datetime.datetime.now())
				print 'Running query: '+str(index)
				payload = open(index + '.json')
				response = requests.get(uri, data=payload, headers=headers)
				format_results_raw_state(index, response.json(), timestamp_start)
		if uri == URI_FTS_RAW_START:
			for index in TYPES_FTS_RAW_START:
				timestamp_start = str(datetime.datetime.now())
				print 'Running query: '+str(index)
				payload = open(index + '.json')
				response = requests.get(uri, data=payload, headers=headers)
				format_results_raw_start(index, response.json(), timestamp_start)
		if uri == URI_FTS_RAW_QUEUE:
                        for index in TYPES_FTS_RAW_QUEUE:
				timestamp_start = str(datetime.datetime.now())
				print 'Running query: '+str(index)
				payload = open(index + '.json')
				response = requests.get(uri, data=payload, headers=headers)
				format_results_raw_queue(index, response.json(), timestamp_start)

def format_results_raw_state(index, response, timestamp_start):
	if index == TYPE_FTS_STAGING_GENERAL:
		format_fts_staging_general(index, response, timestamp_start)
        if index == TYPE_FTS_STAGING_ERROR_REASON:
		format_fts_staging_error_reason(index, response, timestamp_start)
	if index == TYPE_FTS_QUEUE_SUBMITTED:
		format_fts_queue_submitted(index, response, timestamp_start)
	if index == TYPE_FTS_QUEUE_TIME:
                format_fts_queue_time(index, response, timestamp_start)

def format_results_raw_start(index, response, timestamp_start):
	if index == TYPE_FTS_WRITE_TAPE:
		format_fts_write_tape(index, response, timestamp_start)

def format_results_raw_queue(index, response, timestamp_start):
        if index == TYPE_FTS_OPTIMIZER:
                format_fts_optimizer(index, response, timestamp_start)

def format_fts_staging_general(index, response, timestamp_start):
	#print json.dumps(response, indent=4, sort_keys=True)
	docs=[]
	for f in response['aggregations']['per_hour']['buckets']:
               	for file_state in f ['per_file_state']['buckets']: 
                	for vo in file_state ['per_vo']['buckets']:
                        	for endpnt in vo ['per_endpnt']['buckets']:
                                	for source_se in endpnt ['per_source_se']['buckets']:	
						for stage in ['stage', 'transfer']:
							my_info = source_se['stage_only']['buckets'][stage]
							if my_info['doc_count'] != 0:
								my_dict = dict()
                						my_dict['metadata'] = dict()
               						        my_dict['metadata']['event_timestamp'] = f['key']
                						my_dict['data']=dict()
                						my_dict['data']['file_state'] = file_state['key']
								my_dict['data']['vo'] = vo['key']
								my_dict['data']['endpnt'] = endpnt['key']
								my_dict['data']['source_se'] = source_se['key']
								my_dict['data']['timestamp_start'] = timestamp_start
								my_dict['data']['stage_only'] = (stage == 'stage') 
                                        			my_dict['data']['avg_file_size'] = my_info['avg_filesize']['value']
                                        			my_dict['data']['unique_file_id'] = my_info['unique_files']['value']
                                        			my_dict['data']['sum_file_size'] = my_info['sum_filesize']['value']
								my_dict['data']['max_file_size'] = my_info['max_filesize']['value']
								my_dict['data']['min_file_size'] = my_info['min_filesize']['value']
								my_dict['data']['min_staging_duration'] = my_info['min_duration']['value']
								my_dict['data']['max_staging_duration'] = my_info['max_duration']['value']
								my_dict['data']['avg_staging_duration'] = my_info['avg_duration']['value']
								my_dict['data']['staging_duration'] = my_info['sum_finished']['value'] - my_info['sum_started']['value']
                                        			my_dict['data']['count'] = my_info['doc_count']
								my_dict['id'] = str(my_dict['metadata']['event_timestamp']) + '_' + my_dict['data']['file_state'] +'_'+ my_dict['data']['vo']+'_'+ my_dict['data']['endpnt']+'_'+my_dict['data']['source_se']+stage
								my_dict['id'] = my_dict['id'].replace('/', '').replace(':','').replace('\n', '')
                                        			my_dict['data']['timestamp_finish'] = str(datetime.datetime.now())
								docs.append(my_dict)
	data=json.dumps(docs)
	send_data(docs, index)

def format_fts_staging_error_reason(index, response, timestamp_start):
	#print json.dumps(response, indent=4, sort_keys=True)
	docs=[]
        for f in response['aggregations']['per_hour']['buckets']:
		for vo in f ['per_vo']['buckets']: 
                        for endpnt in vo ['per_endpnt']['buckets']: 
				for source_se in endpnt ['per_source_se']['buckets']:	
					for reason in source_se ['per_reason']['buckets']:
						my_dict = dict()
                				my_dict['metadata'] = dict()
                				my_dict['metadata']['event_timestamp'] = f['key']
                				my_dict['data']=dict()
                				my_dict['data']['timestamp_start'] = timestamp_start
						my_dict['data']['vo'] = vo['key']
						my_dict['data']['endpnt'] = endpnt['key']
						my_dict['data']['source_se'] = source_se['key']
						my_dict['data']['reason'] = reason['key']
						my_dict['data']['count'] = reason['doc_count']
						my_dict['id'] = str(my_dict['metadata']['event_timestamp']) + '_' +  my_dict['data']['vo']+'_'+ my_dict['data']['endpnt']+'_'+my_dict['data']['source_se']+'_'+my_dict['data']['reason']
						my_dict['id'] = my_dict['id'].replace('/', '').replace(':','').replace('\n', '')
						my_dict['data']['timestamp_finish'] = str(datetime.datetime.now())
                                		docs.append(my_dict)
	data=json.dumps(docs)
        #print data
	send_data(docs, index)
	return


        
def format_fts_queue_submitted(index, response, timestamp_start):
        #print json.dumps(response, indent=4, sort_keys=True)
        docs=[]
        for f in response['aggregations']['per_hour']['buckets']:
                for vo in f ['per_vo']['buckets']:
                        for endpnt in vo ['per_endpnt']['buckets']:
                                for src in endpnt ['per_source_se']['buckets']:
                                	for dst in src ['per_dest_se']['buckets']:
                                        	my_dict = dict()
                                                my_dict['metadata'] = dict()
                                               	my_dict['metadata']['event_timestamp'] = f['key']
                                                my_dict['data']=dict()
                                                my_dict['data']['timestamp_start'] = timestamp_start
                                                my_dict['data']['vo'] = vo['key']
                                                my_dict['data']['endpnt'] = endpnt['key']
                                                my_dict['data']['source_se'] = src['key']
                                                my_dict['data']['dest_se'] = dst['key']
                                                my_dict['data']['unique_file_id'] = dst['unique_files']['value']
                                                my_dict['data']['count'] = dst['doc_count']
                                                my_dict['id'] = str(my_dict['metadata']['event_timestamp']) + '_' +  my_dict['data']['vo']+'_'+ my_dict['data']['endpnt']+'_'+my_dict['data']['source_se']+'_'+my_dict['data']['dest_se']
                                                my_dict['id'] = my_dict['id'].replace('/', '').replace(':','').replace('\n', '')
                                                my_dict['data']['timestamp_finish'] = str(datetime.datetime.now())
                                                docs.append(my_dict)
        data=json.dumps(docs)
        send_data(docs, index)
        return

def format_fts_queue_time(index, response, timestamp_start):
        #print json.dumps(response, indent=4, sort_keys=True)
        docs=[]
        for f in response['aggregations']['per_hour']['buckets']:
                for vo in f ['per_vo']['buckets']:
                        for endpnt in vo ['per_endpnt']['buckets']:
                                for src in endpnt ['per_source_se']['buckets']:
                                        for dst in src ['per_dest_se']['buckets']:
                                                my_dict = dict()
                                                my_dict['metadata'] = dict()
                                                my_dict['metadata']['event_timestamp'] = f['key']
                                                my_dict['data']=dict()
                                                my_dict['data']['timestamp_start'] = timestamp_start
                                                my_dict['data']['vo'] = vo['key']
                                                my_dict['data']['endpnt'] = endpnt['key']
                                                my_dict['data']['source_se'] = src['key']
                                                my_dict['data']['dest_se'] = dst['key']
                                                my_dict['data']['sum_submit_time'] = dst['sum_submit_time']['value']
						my_dict['data']['sum_timestamp'] = dst['sum_timestamp']['value']
						my_dict['data']['sum_queue_time'] = my_dict['data']['sum_timestamp'] - my_dict['data']['sum_submit_time']
                                                my_dict['data']['max_queue_time_duration'] = dst['max_duration']['value']
						my_dict['data']['min_queue_time_duration'] = dst['min_duration']['value']
						my_dict['data']['avg_queue_time_duration'] = dst['avg_duration']['value']
						my_dict['data']['sum_filesize'] = dst['sum_filesize']['value']
						my_dict['data']['avg_filesize'] = dst['avg_filesize']['value']
						my_dict['data']['max_filesize'] = dst['max_filesize']['value']
						my_dict['data']['min_filesize'] = dst['min_filesize']['value']
						my_dict['data']['count'] = dst['doc_count']
                                                my_dict['id'] = str(my_dict['metadata']['event_timestamp']) + '_' +  my_dict['data']['vo']+'_'+ my_dict['data']['endpnt']+'_'+my_dict['data']['source_se']+'_'+my_dict['data']['dest_se']
                                                my_dict['id'] = my_dict['id'].replace('/', '').replace(':','').replace('\n', '')
                                                my_dict['data']['timestamp_finish'] = str(datetime.datetime.now())
                                                docs.append(my_dict)
        data=json.dumps(docs)
        send_data(docs, index)
        return

def format_fts_write_tape(index, response, timestamp_start):
	#print json.dumps(response, indent=4, sort_keys=True)
        docs=[]
        for f in response['aggregations']['per_hour']['buckets']:
              	for vo in f ['per_vo']['buckets']: 
                        for endpnt in vo ['per_endpnt']['buckets']:
                                for scope in endpnt ['per_scope']['buckets']:
                                       for src in scope ['per_src']['buckets']:
					   	for dst in src ['per_dst']['buckets']:
						       my_dict = dict()
               					       my_dict['metadata'] = dict()
                				       my_dict['metadata']['event_timestamp'] = f['key']
                				       my_dict['data']=dict()
                				       my_dict['data']['timestamp_start'] = timestamp_start
                                                       my_dict['data']['vo'] = vo['key']
						       my_dict['data']['endpnt'] = endpnt['key']
						       my_dict['data']['scope'] = scope['key']
						       my_dict['data']['source_hostname'] = src['key']
						       my_dict['data']['dest_hostname'] = dst['key']
                                                       my_dict['data']['sum_file_size'] = dst['sum_filesize']['value']
                                                       my_dict['data']['count'] = dst['doc_count']
                                                       my_dict['id'] = str(my_dict['metadata']['event_timestamp']) + '_' +  my_dict['data']['vo']+'_'+ my_dict['data']['endpnt']+'_'+my_dict['data']['source_hostname']+'_'+my_dict['data']['dest_hostname']+'_'+my_dict['data']['scope']
                                                       my_dict['id'] = my_dict['id'].replace('/', '').replace(':','').replace('\n', '')
                    				       my_dict['data']['timestamp_finish'] = str(datetime.datetime.now())
			            		       docs.append(my_dict)
        data=json.dumps(docs)
        send_data(docs, index)
	return


def format_fts_optimizer(index, response, timestamp_start):
        #print json.dumps(response, indent=4, sort_keys=True)
        docs=[]
        for f in response['aggregations']['per_hour']['buckets']:
                for endpnt in f ['per_endpnt']['buckets']:
                	for src in endpnt ['per_source_se']['buckets']:
                         	for dst in src ['per_dest_se']['buckets']:
                                	my_dict = dict()
                                        my_dict['metadata'] = dict()
                                        my_dict['metadata']['event_timestamp'] = f['key']
                                        my_dict['data']=dict()
                                        my_dict['data']['timestamp_start'] = timestamp_start
                                        my_dict['data']['endpnt'] = endpnt['key']
                                        my_dict['data']['source_se'] = src['key']
					my_dict['data']['dest_se'] = dst['key']
                                        my_dict['data']['avg_file_size'] = dst['avg_file_size']['value']
					my_dict['data']['avg_throughput'] = dst['avg_throughput']['value']
					my_dict['data']['avg_throughput_ema'] = dst['avg_throughput_ema']['value']
					my_dict['data']['avg_connections'] = dst['avg_connections']['value']
					my_dict['data']['avg_success_rate'] = dst['avg_success_rate']['value']
                                        my_dict['data']['count'] = dst['doc_count']
                                        my_dict['id'] = str(my_dict['metadata']['event_timestamp']) + '_'+ my_dict['data']['endpnt']+'_'+my_dict['data']['source_se']+'_'+my_dict['data']['dest_se']
                                        my_dict['id'] = my_dict['id'].replace('/', '').replace(':','').replace('\n', '')
                                        my_dict['data']['timestamp_finish'] = str(datetime.datetime.now())
                                        docs.append(my_dict)
        data=json.dumps(docs)
        send_data(docs, index)
        return

def send_data(data, index):
	
	
        certpath = "/etc/pki/tls/certs/ca-bundle.trust.crt"
	es = Elasticsearch(
    	'https://'+USER_ES+':'+PASSWORD_ES+'@es-ftsmon6.cern.ch/es',
    	# turn on SSL
    	use_ssl=True,
   	 # make sure we verify SSL certificates (off by default)
    	verify_certs=True,
	ca_certs=certpath
	)
	requestBody = ""
        for d in data:
	   requestBody+='{"index":{"_id": "'+d['id']+'"}}\n'
           requestBody+= json.dumps(d) +"\n"
	now = datetime.datetime.now()
	#print requestBody	
	try:
		es.bulk(index='ftsmon_'+index+'-'+str(now.year), doc_type=index, body=requestBody)
	except TransportError as e:
		print e.info
	return

if __name__ == '__main__':
	launch_queries()			
